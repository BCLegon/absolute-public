# Absolute — Privacy Policy

Last updated: June 12, 2024

Thank you for using Absolute. This Privacy Policy explains our practices regarding the collection, use, and disclosure of information that we receive through our app. By using Absolute, you agree to the terms outlined in this Privacy Policy.

## 1. Information Collection

Absolute does not collect any personal data from its users. We do not track, store, or share any personal information.

## 2. Data Usage

As Absolute does not collect any personal data, there is no data to use, store, or share. The only information that might be collected is handled by Apple through the App Store, as per their own privacy policies and practices.

## 3. Third-Party Services

Absolute does not use any third-party services or trackers. The only data that might be collected is by the App Store itself, as part of its standard operation. Please refer to Apple's privacy policy for details on how they handle your data: [Apple Privacy Policy](https://www.apple.com/legal/privacy/).

## 4. Changes to This Privacy Policy

We may update our Privacy Policy from time to time. Any changes will be posted on this page. You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.
